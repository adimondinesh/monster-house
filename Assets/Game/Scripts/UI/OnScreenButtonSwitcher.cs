using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.OnScreen;
using UnityEngine.UI;

public class OnScreenButtonSwitcher : MonoBehaviour
{
    public Button pickupBtn;        // Pickup UI Button
    public Button throwBtn;         // Throw UI Button

    GameActions gameActions;

    private void Awake()
    {
        gameActions = new GameActions();
        gameActions.Action.Pickup.performed += PickupBtnPressed;
        gameActions.Action.Throw.performed += ThrowBtnPressed;
    }

    // Enable Throw button when Pickup Button is pressed
    private void PickupBtnPressed(InputAction.CallbackContext obj)
    {
        pickupBtn.gameObject.SetActive(false);
        throwBtn.gameObject.SetActive(true);
    }

    // Enable Pickup button when Throw Button is pressed
    private void ThrowBtnPressed(InputAction.CallbackContext obj)
    {
        pickupBtn.gameObject.SetActive(true);
        throwBtn.gameObject.SetActive(false);
    }


    void OnEnable()
    {
        gameActions.Enable();
    }

    void OnDisable()
    {
        gameActions.Disable();
    }

    private void OnDestroy()
    {
        gameActions.Dispose();
    }
}
