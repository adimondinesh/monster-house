using UnityEngine;
using static GenDefs;

public class ThrowableController : MonoBehaviour
{
    public ThrowableType type;

    Rigidbody rb;                       
    PlayerController player;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        player = GameManager.Instance.player;
    }

    // Resets properties before its spawned from the object pool
    public void SetSpawnProperties()
    {
        rb.isKinematic = true;
        transform.rotation = Quaternion.Euler(Vector3.zero);
        rb.useGravity = false;
        GetComponent<SphereCollider>().isTrigger = true;
    }

    // Picking up the Throwable
    public void Pickup()
    {
        rb.velocity = Vector3.zero;
        rb.isKinematic = true;
        rb.useGravity = false;
        transform.SetParent(player.throwableHolder);
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.Euler(Vector3.zero);
        GetComponent<SphereCollider>().isTrigger = true;
    }

    // Throwing the Throwable
    public void Throw()
    {
        rb.isKinematic = false;
        rb.useGravity = true;
        transform.SetParent(null);
        GetComponent<SphereCollider>().isTrigger = false;

        rb.velocity = player.throwableHolder.transform.forward * player.throwForce;
        rb.velocity += (player.GetComponent<Rigidbody>().velocity * 0.5f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        
        ObjectPooler.Instance.DestroyThrowable(type, this.gameObject);
        SetSpawnProperties();       // Sets this object properties to its initial values and sends them to pool to be used later
        ObjectPooler.Instance.SpawnThrowable();
    }
}
