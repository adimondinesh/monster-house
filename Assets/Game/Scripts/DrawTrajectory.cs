using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawTrajectory : MonoBehaviour
{
    PlayerController player;
    LineRenderer lineRenderer;

    // Number of points on the line
    public int numPoints = 50;

    // distance between those points on the line
    public float timeBetweenPoints = 0.1f;

    // The physics layers that will cause the line to stop being drawn
    public LayerMask CollidableLayers;

    bool drawGizmos;

    void Start()
    {
        player = GameManager.Instance.player;
        lineRenderer = GetComponent<LineRenderer>();
        EnableTrajectory(false);
        drawGizmos = true;
    }

    void Update()
    {
        DrawTrajection();
    }

    void DrawTrajection()
    {
        lineRenderer.positionCount = numPoints;
        List<Vector3> points = new List<Vector3>();
        Vector3 startingPosition = player.throwableHolder.position;
        Vector3 startingVelocity = player.throwableHolder.forward * player.throwForce;
        for (float t = 0; t < numPoints; t += timeBetweenPoints)
        {
            Vector3 newPoint = startingPosition + t * startingVelocity;
            newPoint.y = startingPosition.y + startingVelocity.y * t + Physics.gravity.y / 2f * t * t;
            points.Add(newPoint);

            if (Physics.OverlapSphere(newPoint, 0.05f, CollidableLayers).Length > 0)
            {
                lineRenderer.positionCount = points.Count;
                break;
            }
        }

        lineRenderer.SetPositions(points.ToArray());
    }

    // Controls visibility of Line Renderer
    public void EnableTrajectory(bool enable = true)
    {
        lineRenderer.enabled = enable;
    }

    private void OnDrawGizmos()
    {
        if (drawGizmos)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawSphere(player.throwableHolder.position, 0.1f);
        }
    }
}
