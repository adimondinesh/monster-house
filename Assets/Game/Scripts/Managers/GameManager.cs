using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using static GenDefs;

public class GameManager : Singleton<GameManager>
{
    public PlayerController player;
    public CameraController cameraController;
    public bool drawGizmos;

    [HideInInspector]
    public ControlDevice controlDevice;
    [HideInInspector]
    public List<ThrowableController> throwablesList;

    private void Awake()
    {
        SetControlScheme();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Change controls based on current device
    void SetControlScheme()
    {
        if (Touchscreen.current == null)
        {
            controlDevice = ControlDevice.PC;
            return;
        }
        controlDevice = ControlDevice.Mobile;
    }

    // Finds the closest Throwable from the player within the given pickup range
    public ThrowableController FindNearestThrowable(float pickupRange)
    {
        ThrowableController closestThrowable = null;
        float closestDistanceSqr = Mathf.Pow(pickupRange, 2);

        foreach (var throwable in throwablesList)
        {
            Vector3 directionToTarget = throwable.transform.position - player.transform.position;
            float dirSqrToTarget = directionToTarget.sqrMagnitude;
            if (dirSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dirSqrToTarget;
                closestThrowable = throwable;
            }
        }

        return closestThrowable;
    }
}
