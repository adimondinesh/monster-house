using System;
using UnityEngine;
using UnityEngine.InputSystem;
using static GenDefs;

public class PlayerController : MonoBehaviour
{
    [Header("Movement")]

    public float walkSpeed;
    public float slowWalkSpeed;
    public float movementSmoothingTime = 0.05f;
    public Transform orientation;

    [Header("Ground Check")]

    public float playerHeight;
    public LayerMask groundLayer;
    public float groundDrag;
    bool grounded;

    [Header("Throwable")]

    public Transform throwableHolder;
    public float throwForce;                // Force for throwing the throwable
    public float pickUpRange;               // Distance between player and throwable when the player can pick it up


    float moveSpeed;
    Vector2 inputVec;
    Vector3 moveDirection;
    Vector3 refVel = Vector3.zero;                     // Reference Velocity for Movement Smoothing
    Quaternion initialAimRotation;
    Rigidbody rb;
    GameActions gameActions;
    DrawTrajectory drawTrajectory;                     // Draws a Trajectory path for the throwablew
    ThrowableController throwableInHand = null;        // Active throwable picked up by the player
    
    private void Awake()
    {
        //// Check for devices that are added, removed or updated
        //InputSystem.onDeviceChange +=
        //(device, change) =>
        //{
        //    switch (change)
        //    {
        //        case InputDeviceChange.Added:
        //            Debug.Log("New device added: " + device.displayName);
        //            break;

        //        case InputDeviceChange.Removed:
        //            Debug.Log("Device removed: " + device);
        //            break;

        //        case InputDeviceChange.UsageChanged:
        //            SetControlScheme();
        //            break;
        //    }
        //};

        gameActions = new GameActions();
        gameActions.Action.Aim.performed += EnableAim;
        gameActions.Action.Aim.canceled += DisableAim;
        gameActions.Action.Pickup.performed += PickAction;
        gameActions.Action.Throw.performed += ThrowAction;
        if (GameManager.Instance.controlDevice != ControlDevice.Mobile)
        {
            gameActions.Movement.SlowWalk.performed += SlowWalkPressed;
            gameActions.Movement.SlowWalk.canceled += SlowWalkCancelled;
        }
    }

    

    private void OnEnable()
    {
        gameActions.Enable();
    }

    private void OnDisable()
    {
        gameActions.Disable();
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
        moveSpeed = walkSpeed;
        initialAimRotation = throwableHolder.rotation;          // store default throw angle
        drawTrajectory = GetComponent<DrawTrajectory>();
    }

    

    void FixedUpdate()
    {
        Move();
    }

    void Update()
    {
        GroundCheck();
        GetInput();
    }

    void GroundCheck()
    {
        // ground check
        grounded = Physics.Raycast(transform.position, Vector3.down, playerHeight * 0.5f + 0.2f, groundLayer);

        // handle Drag
        rb.drag = grounded ? groundDrag : 0;
    }

    void GetInput()
    {
        inputVec = gameActions.Movement.Move.ReadValue<Vector2>();
    }

    void Move()
    {
        moveDirection = orientation.forward * inputVec.y + orientation.right * inputVec.x;
        rb.AddForce(moveDirection.normalized * moveSpeed * Time.deltaTime, ForceMode.Force);

        // Movement smoothing and capping speed to target speed
        Vector3 targetVel = moveDirection * moveSpeed;
        rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVel, ref refVel, movementSmoothingTime);
    }

    // Slow Walk is triggered
    void SlowWalkPressed(InputAction.CallbackContext callback)
    {
        moveSpeed = slowWalkSpeed;
    }

    // Slow Walk is Cancelled/Released
    void SlowWalkCancelled(InputAction.CallbackContext callback)
    {
        moveSpeed = walkSpeed;
    }

    // Method to toggle SlowWalk on Touch Screen
    public void ToggleSlowWalk()
    {
       if(moveSpeed == slowWalkSpeed)
       {
            moveSpeed = walkSpeed;
            return;
       }
        moveSpeed = slowWalkSpeed;
    }

    // Aim is Enabled
    private void EnableAim(InputAction.CallbackContext callback)
    {
        // Aim only if a throwable is picked up
        if (throwableHolder.childCount > 0)     
        {
            GameManager.Instance.cameraController.SetCamera(CameraStyle.Combat);
            drawTrajectory.EnableTrajectory();
        }
    }

    // Aim is Disabled 
    private void DisableAim(InputAction.CallbackContext callback)
    {
        // Reset Aim to default throw angle when not Aiming manually
        GameManager.Instance.cameraController.SetCamera(CameraStyle.Basic);
        throwableHolder.rotation = Quaternion.Euler(new Vector3(initialAimRotation.eulerAngles.x, throwableHolder.rotation.eulerAngles.y, throwableHolder.rotation.eulerAngles.z));
        drawTrajectory.EnableTrajectory(false);
    }

    // Pick Throwable
    public void PickAction(InputAction.CallbackContext callback)
    {
        if (throwableInHand == null)
        {
            ThrowableController nearestThrowable = GameManager.Instance.FindNearestThrowable(pickUpRange);
            if(nearestThrowable == null)
            {
                Debug.Log("No Throwable within pickup Range");
                return;
            }
            throwableInHand = nearestThrowable;
            throwableInHand.Pickup();
        }
    }

    // Throw picked Throwable
    private void ThrowAction(InputAction.CallbackContext callback)
    {
        if(throwableInHand != null)
        {
            throwableInHand.Throw();
            throwableInHand = null;
            DisableAim(callback);
        }
    }

    private void OnDestroy()
    {
        gameActions.Movement.SlowWalk.performed -= SlowWalkPressed;
        gameActions.Movement.SlowWalk.canceled -= SlowWalkCancelled;
        gameActions.Dispose();
    }
}
