using Cinemachine;
using UnityEngine;
using static GenDefs;

public class CameraController : Singleton<CameraController>
{
    [Header("Camera References")]
    public CinemachineVirtualCameraBase basicCam;
    public CinemachineVirtualCameraBase combatCam;


    [Header("Player References")]
    public Transform orientation;
    public Transform combatLookAt;
    public Transform player;
    public Transform playerGFX;
    public Rigidbody rb;

    public float rotationSpeed;
    public float yAimOffset;

    GameActions gameActions;

    CameraStyle cameraStyle;

    void Awake()
    {
        gameActions = new GameActions();
    }

    void OnEnable()
    {
        gameActions.Enable();
    }
    void OnDisable()
    {
        gameActions.Disable();
    }

    void Start()
    {
        SetCamera(CameraStyle.Basic);
        CinemachineCore.GetInputAxis = GetAxisNew; //override cinemachine axis input using input system

        //Cursor.lockState = CursorLockMode.Locked;
        //Cursor.visible = false;
    }

    void Update()
    {
        CalculateOrientation();       
    }

    void CalculateOrientation()
    {
        // rotate orientation
        Vector3 viewDir = player.position - new Vector3(transform.position.x, player.position.y, transform.position.z);
        orientation.forward = viewDir.normalized;

        if (cameraStyle == CameraStyle.Basic)
        {
            // roate player object
            Vector2 inputVec = gameActions.Movement.Move.ReadValue<Vector2>();
            Vector3 inputDir = orientation.forward * inputVec.y + orientation.right * inputVec.x;

            if (inputDir != Vector3.zero)
            {
                playerGFX.forward = Vector3.Slerp(playerGFX.forward, inputDir.normalized, Time.deltaTime * rotationSpeed);
            }
        }

        else if (cameraStyle == CameraStyle.Combat)
        {
            Vector3 dirToCombatLookAt = combatLookAt.position - new Vector3(transform.position.x, combatLookAt.position.y, transform.position.z);
            orientation.forward = dirToCombatLookAt.normalized;

            playerGFX.forward = dirToCombatLookAt.normalized;

            Vector3 dirToAimThrowable = combatLookAt.position - new Vector3(transform.position.x, transform.position.y - yAimOffset, transform.position.z);
            //GameManager.Instance.player.throwableHolder.forward = Vector3.Slerp(GameManager.Instance.player.throwableHolder.forward, dirToAimThrowable.normalized, Time.deltaTime * rotationSpeed); 
            GameManager.Instance.player.throwableHolder.forward = dirToAimThrowable;
        }

    }

    float GetAxisNew(string pAxisName)
    {
        Vector2 aLookDelta = gameActions.Camera.Look.ReadValue<Vector2>();
        
        aLookDelta.Normalize();
        if (pAxisName == "Mouse X")
        {
            return aLookDelta.x;
        }
        else if (pAxisName == "Mouse Y")
        {
            return aLookDelta.y;
        }
        return 0;
    }

    public void SetCamera(CameraStyle style)
    {
        if(style == CameraStyle.Basic)
        {

            basicCam.Priority = 10;
            combatCam.Priority = 1;
            combatCam.gameObject.SetActive(false);
            basicCam.gameObject.SetActive(true);

            cameraStyle = CameraStyle.Basic;
            return;
        }

        basicCam.Priority = 1;
        combatCam.Priority = 10;
        combatCam.gameObject.SetActive(true);
        basicCam.gameObject.SetActive(false);

        cameraStyle = CameraStyle.Combat;
    }

    private void OnDestroy()
    {
        gameActions.Dispose();
    }
}
