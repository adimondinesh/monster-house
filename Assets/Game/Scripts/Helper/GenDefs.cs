using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenDefs : MonoBehaviour
{
    [System.Serializable]
    public enum ControlDevice                          // To Check which device the PLayer is using
    {
        PC,
        Mobile
    };

    [System.Serializable]
    public enum CameraStyle                            // Camera Style for Player 
    {
        Basic = 0,
        Combat = 1
    }

    [System.Serializable]
    public enum ThrowableType                          // Type of Throwable
    {
        Basic
    }
}
