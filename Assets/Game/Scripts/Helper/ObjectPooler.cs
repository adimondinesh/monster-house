using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using static GenDefs;

public class ObjectPooler : Singleton<ObjectPooler>
{
    public Transform ground;                    // Centre of Spawn Area
    public float spawnRange;                    // Radius of Spawn Area 
    public float spawnHeight;                   // Spawn Height
    public float spawnAmount;                   // Spawn Amount

    Vector3 spawnOffset;                        // Spawn offset based on spawnHeight

    //-- Object Pool Variables --//

    [System.Serializable]
    public class Pool
    {
        public GameObject prefab;
        public ThrowableType type;
        public int amount;
    }

    public List<Pool> pools;                                         // List to Get Pool data from user                               
    Dictionary<ThrowableType, Queue<GameObject>> poolDictionary;     // Dictionary to Store all Pool data

    //-- --//

    // Instatitate all members in the Pool
    void Awake()
    {
        poolDictionary = new Dictionary<ThrowableType, Queue<GameObject>>();

        foreach (Pool entry in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();

            for (int i = 0; i < entry.amount; i++)
            {
                GameObject obj = Instantiate(entry.prefab);
                obj.SetActive(false);
                objectPool.Enqueue(obj);

                // Storing All Throwable Data in GameManager for future calculations
                GameManager.Instance.throwablesList.Add(obj.GetComponent<ThrowableController>());
            }

            poolDictionary.Add(entry.type, objectPool);
        }
    }

    void Start()
    {
        for (int i = 0; i < spawnAmount; i++)
        {
            SpawnThrowable();
        }
    }

    // Spawn throwable at a random location in the spawn area
    public void SpawnThrowable()
    {
        bool objectHasSpawned = false;          // To check if an object has been spawned

        // Check if the random spot is on the ground and not being obstructed
        while (!objectHasSpawned)
        {
            Vector3 randomPoint = ground.position + Random.insideUnitSphere * spawnRange; // get a random spot from the spawn area

            if (NavMesh.SamplePosition(randomPoint, out NavMeshHit hit, 2f, NavMesh.AllAreas))
            {
                // Spawn a throwable and get its reference to set spawn properties
                SpawnFromPool(ThrowableType.Basic, hit.position + spawnOffset);
                objectHasSpawned = true;
            }
        }
    }

    // Send the throwable back to its pool 
    public void DestroyThrowable(ThrowableType type, GameObject throwableObj)
    {
        SendToPool(type , throwableObj);
    }


    // Spawns an Object from the respective pool and sets its position, and enables it in the scene
    public GameObject SpawnFromPool(ThrowableType type, Vector3 position)
    {
        if (!poolDictionary.ContainsKey(type))
        {
            Debug.LogWarning("Pool with Color " + type + " doesn't exist!");
            return null;
        }

        GameObject objectToSpawn = poolDictionary[type].Dequeue();

        objectToSpawn.SetActive(true);
        objectToSpawn.transform.position = position;


        return objectToSpawn;
    }

    // Returns an object back to the respective pool, resets position and disables it in the scene
    public void SendToPool(ThrowableType type, GameObject obj)
    {
        if (!poolDictionary.ContainsKey(type))
        {
            Debug.LogWarning("Pool with Color " + type + " doesn't exist!");
            return;
        }

        obj.SetActive(false);
        obj.transform.position = transform.position;
        poolDictionary[type].Enqueue(obj);
    }

    private void OnDrawGizmos()
    {
        if (GameManager.Instance.drawGizmos)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawSphere(ground.transform.position, spawnRange);
        }
    }
}
